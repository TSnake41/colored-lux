# LÖVE experimental 2D colored shadow.

### Settings

You can change few settings in main.lua.

Shadow resolution multiplier :
 - 0.10 : Very low
 - 0.25 : Low
 - 0.50 : Medium
 - 0.75 : High
 - 1.00 : Ultra

 The higher the shadow resolution, the better the quality, but the higher (!!!) the GPU power required.
 Medium looks good in most cases and works nicely on a GTX 560-Ti with few shadows (4-5).

 High and Ultra are very heavy but generates very sharp shadows, values bigger
 than 1.00 generates supersampled shadows which are impresively heavy, only
 try it if you have a overpowered computer.

Light size :
  The bigger the light size, the bigger the cost as the shadow quality is
  constant between light size, you can still scale the resolution multiplier
  to get a constant cost regardless the size of the light but quality may suffers.

### Notes

The current implementation is based on [this tutorial](https://github.com/mattdesl/lwjgl-basics/wiki/2D-Pixel-Perfect-Shadows)
with few extensions (like colored shadows) and is very heavy as it is not optimizable
with classical OpenGL and may need the use of OpenGL DSA or Vulkan to get way far better
performance.

By using column of pixels instead of pixels in vertical_proj.frag, we can apply
the projection once per column and store sucessives results in a sampler1D from
a sampler1D (the column).

Using the current algorithm, the projection count is :
 - size * size / 2 (/ 2 as some pixels aren't doing a full projection)

While with the optimized projection algorithm, this projection count is :
 - size

Therefore, for a 512x512 light :
  - old : 512x256 = 131 072 projections
  - new : 512 projections

Which might produce a speedup of x256 in optimal case.

The situation is even worse for 2048x2048 lights :
  - old : 2048 * 1024 = 2 097 152 projections
  - new : 2048

Which might produce a speedup of x1024 (!!!) in optimal case.
