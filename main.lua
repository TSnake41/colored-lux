local Light = require "Light"

local default_canvas
local draw_cubes = false

local squares = {}
local lights = {}

-- 0.1 : Very low
-- 0.25 : Low
-- 0.50 : Medium
-- 0.75 : High
-- 1.0 : Ultra

local light_size = 1024
local resolution = 0.75

function love.load()
  default_canvas = love.graphics.newCanvas()
  lights[1] = Light(0, 0, light_size, resolution)

  --shader:send("strengh", strengh)

  --[[squares[1] = {
    (love.graphics.getWidth() - light_size) / 2,
    (love.graphics.getHeight() - light_size) / 2,
    { 1, 1, 1, 1 }
  }]]



  --[[for x=0,40 do
    for y=0,30 do
      squares[#squares + 1] = {
        32 * x, 32 * y,
        { math.random(), math.random(), math.random(), 1.0 }
      }
    end
  end]]


  for i=1,50 do
    squares[i] = {
      64 * math.ceil(math.random(1, 20)),
      64 * math.ceil(math.random(1, 15)),
      { math.random(), math.random(), math.random(), 1 }
    }
  end
  end

function love.update(dt)
  love.window.setTitle(string.format("FPS: %d | Memory : %g kb | Draw map : %s",
    love.timer.getFPS(), math.floor(collectgarbage 'count'), tostring(draw_cubes)))

  px, py = love.mouse.getPosition()
  lights[#lights].x, lights[#lights].y = px - light_size / 2, py - light_size / 2
end

function love.draw()
  love.graphics.setShader()

  love.graphics.setCanvas(default_canvas)
  love.graphics.clear()

  for i,square in ipairs(squares) do
    love.graphics.setColor(square[3])
    love.graphics.rectangle("fill", square[1], square[2], 64, 64)
    love.graphics.setColor(1, 1, 1, 1)
  end

  love.graphics.setCanvas()

  love.graphics.clear(0, 0, 0, 1)

  if draw_cubes then
    love.graphics.draw(default_canvas)
  end

  for i=1,#lights do
    lights[i]:render(default_canvas)
  end
end

function love.keypressed(key, scancode, isrepeat)
  if key == "c" then
    draw_cubes = not draw_cubes
  end

  if key == 'l' then
    lights[#lights + 1] = Light(0, 0, light_size, resolution)
  end
end
