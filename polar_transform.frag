#define PI 3.141592

vec4 effect(vec4 color, sampler2D tex, vec2 uv, vec2 sc)
{
  float theta = uv.x * PI * 2.0f;
  float r = uv.y / 2.0f;

  vec2 new_uv = r * vec2(cos(theta), sin(theta)) + .5f;

  vec4 texcolor = texture2D(tex, new_uv);
  return texcolor * color;
}
