vec4 effect(vec4 color, sampler2D tex, vec2 uv, vec2 px)
{
  /* Do a vertical shadow projection. */
  float y_step = 1 / love_ScreenSize.y;

  vec4 final_color = vec4(1.0f);

  for(float y = uv.y; y > 0.0f; y -= y_step) {
    vec4 texcolor = texture2D(tex, vec2(uv.x + y_step / 2, y + y_step / 2));

    if (texcolor.a > 0.999f) {
      final_color.r = min(final_color.r, texcolor.r);
      final_color.g = min(final_color.g, texcolor.g);
      final_color.b = min(final_color.b, texcolor.b);
      final_color.a = min(final_color.a, texcolor.a);
    }
  }

  return final_color;
}
