#define PI 3.141592

#define LINEAR_LIGHT 0
#define EXPONENTIAL_LIGHT 0
#define LOGARITHMIC_LIGHT 1

float compute_light(float dist)
{
  float coeff = 1.0f;

  #if LINEAR_LIGHT
  coeff = 1 - dist; // linear
  #elif EXPONENTIAL_LIGHT
  coeff = exp(-2 * dist); // exponential
  #elif LOGARITHMIC_LIGHT
  coeff = 0.5f - log(0.625f + dist); // logarithmic
  #endif

  return max(coeff, 0.0f);
}

vec4 effect(vec4 color, sampler2D tex, vec2 uv, vec2 px)
{
  /* Compute light strengh */
  float dist = length(uv - .5f) * 2;

  /* Convert cartesian to polar. */
  /* Tranform coordinates to light center. */
  vec2 light_uv = uv - .5f;

  float theta = atan(light_uv.y, light_uv.x);
  if (theta < 0)
    theta += PI * 2;

  float r = length(light_uv);

  // vec2 new_uv = r * vec2(cos(theta), sin(theta)) + .5f;
  float x = theta / (2.0f * PI);
  float y = r * 2.0f;

  vec4 texcolor = texture2D(tex, vec2(x, y));
  return compute_light(2.0f * r) * texcolor * color;
}
