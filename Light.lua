local Light = require "classic":extend()

local polar_transform = love.graphics.newShader "polar_transform.frag"
local vert_proj = love.graphics.newShader "vertical_proj.frag"
local light_shader = love.graphics.newShader "light.frag"

-- 0.1 : Very low
-- 0.25 : Low
-- 0.50 : Medium
-- 0.75 : High
-- 1.0 : Ultra

function Light:new(x, y, size, resmul)
  self.x = x or 0
  self.y = y or 0

  self.size = size or 256
  self.resmul = resmul or 1

  self.canvas_size = math.ceil(self.size * self.resmul)

  self.canvas1 = love.graphics.newCanvas(self.canvas_size, self.canvas_size)
  self.canvas2 = love.graphics.newCanvas(self.canvas_size, self.canvas_size)
end

function Light:render(render_canvas)
  local old_shader = love.graphics.getShader()

  -- Draw light partition to canvas.
  love.graphics.setCanvas(self.canvas2)
  love.graphics.clear()
  love.graphics.push()
  love.graphics.scale(self.resmul, self.resmul)
  love.graphics.draw(render_canvas, -self.x, -self.y)
  love.graphics.pop()

  love.graphics.setCanvas()
  --love.graphics.draw(self.canvas2, 0, 0)

  -- Do the polar transformation.
  love.graphics.setCanvas(self.canvas1)
  love.graphics.clear()
  love.graphics.setShader(polar_transform)
  love.graphics.draw(self.canvas2)

  love.graphics.setCanvas()
  --love.graphics.draw(self.canvas2, self.canvas_size, 0)

  -- Do the vertical projection.
  love.graphics.setCanvas(self.canvas2)
  love.graphics.clear()
  love.graphics.setShader(vert_proj)
  love.graphics.draw(self.canvas1)

  love.graphics.setCanvas()
  --love.graphics.draw(self.canvas1, self.canvas_size * 2, 0)

  -- Draw the final render.
  love.graphics.setCanvas(self.canvas1)
  love.graphics.clear()
  love.graphics.setShader(light_shader)
  love.graphics.draw(self.canvas2)

  -- Draw the final result
  love.graphics.setCanvas()
  love.graphics.setShader()

  love.graphics.push()
  local old_blendmode = love.graphics.getBlendMode()
  love.graphics.setBlendMode("screen", "premultiplied")
  love.graphics.scale(1 / self.resmul, 1 / self.resmul)
  --love.graphics.draw(self.canvas1, self.canvas_size * 3 * self.resmul, 0)
  love.graphics.draw(self.canvas1, self.x * self.resmul, self.y * self.resmul)
  love.graphics.setBlendMode(old_blendmode)
  love.graphics.pop()

  love.graphics.setShader(old_shader)
end

return Light
